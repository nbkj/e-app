<?php

namespace natEmergency\Http\Controllers\Api\V1;

use natEmergency\User;

use natEmergency\Http\Controllers\Controller;
use natEmergency\Http\Controllers\Api\Traits\UserValidatorTrait;

use Illuminate\Support\Facades\Input;

use natEmergency\Services\UserServices;

class UserLoginController extends Controller 
{
	use UserValidatorTrait;

	public function __construct(UserServices $user)
	{
		$this->user = $user;
	}

	public function login()
	{
		$input = Input::only('email','password','user_type');

		$this->addErrorMsg($this->validateLoginEmail('email',$input));
		$this->addErrorMsg($this->validateString('password',$input));
		$this->addErrorMsg($this->validateUserType('user_type',$input));

		if ($this->getErrorMsg()) {
			$response =
				[
					'status' => 400,
					'data'	=>
						[
							'errors' => $this->getErrorMsg()
						]
				];
			return $response;
		} else
		{
			$login = $this->user->checkLogin($input['email'],$input['password'],$input['user_type']);
			if($login == false){
				$response = 
				[
					'status' => 401,
					'data' => 
					[
						'error' => 'Username and Password Incorrect'
					]
				];

				return $response;
			}else{
				$response = 
					[
						'status' => 200,
						'data' =>
						[
							'success' => $login
						]
					];

					return $response;
			}
		}
	}
}