<?php

namespace natEmergency\Http\COntrollers\Api\Traits;

use Illuminate\Support\Facades\Validator;

trait UserValidatorTrait{

	public $errors = [];

	public function getErrormsg()
	{
		return $this->errors;
	}

	public function setErrorMsg($msg)
	{
		return $this->errors = $msg;
	}
	public function addErrorMsg($msg)
	{
		if(empty($this->errors) && !empty($msg)){

            $this->setErrorMsg([$msg]);

        } elseif(!empty($msg) ) {
            $this->errors = array_merge(
                $this->getErrormsg(),
                [$msg]
            );
        }
	}
	public function validateString($field, $input)
	{
		$validator = Validator::make(
			[
				"$field" => strtolower($input["$field"]),
			],
			[
				"$field" => 'required|string'
			]);

		if($validator->fails())
		{
			$validator_msg = $validator->messages()->toArray();

			return $this->setErrorMsg = [
				"field" => $field,
				"message" => $validator_msg[$field][0]
			];
		}
	}

		public function validateUserType($field, $input)
		{
			$validator = Validator::make(
				[
					"$field" => strtolower($input["$field"]),
				],
				[
					"$field" => 'required|string|in:bfp,hospital,user'
				]);

			if($validator->fails())
			{
				$validator_msg = $validator->messages()->toArray();

				return $this->setErrorMsg = [
					"field" => $field,
					"message" => $validator_msg[$field][0]
				];
			}
		}

	public function validateEmail($field, $input)
	{
		$validator = Validator::make(
			[
				"$field" => strtolower($input["$field"]),
			],
			[
				"$field" => 'required|email|unique:users'
			]);

		if($validator->fails())
		{
			$validator_msg = $validator->messages()->toArray();

			return $this->setErrorMsg = [
				"field" => $field,
				"message" => $validator_msg[$field][0]
			];
		}
	}

	public function validatePassword($field, $input)
	{
		$validator = Validator::make(
			[
				"$field" => strtolower($input["$field"]),
			],
			[
				"$field" => 'required|min:8'
			]);

		if($validator->fails())
		{
			$validator_msg = $validator->messages()->toArray();

			return $this->setErrorMsg = [
				"field" => $field,
				"message" => $validator_msg[$field][0]
			];
		}
	}

	public function validateLoginEmail($field, $input)
	{
		$validator = Validator::make(
			[
				"$field" => strtolower($input["$field"]),
			],
			[
				"$field" => 'required|email'
			]);

		if($validator->fails())
		{
			$validator_msg = $validator->messages()->toArray();

			return $this->setErrorMsg = [
				"field" => $field,
				"message" => $validator_msg[$field][0]
			];
		}
	}
}